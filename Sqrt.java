import java.text.DecimalFormat;
import java.util.stream.Stream;

public class Sqrt {

    private final double esp = 0.000000000012;
    private final double initialValue = 0.000000000001;
    private final int base = 2;

    private int number;

    public Sqrt(int number){

        this.number = number;
    }

    private double aproximateSqrt(int base, double esp, int number){

        Stream<Co> infiniteStreamDouble1 = Stream.iterate(new Co(initialValue, initialValue),
                co -> new Co(co.next,(co.next + (number/co.next))/base)).skip(1);

        Co sqrt = infiniteStreamDouble1.filter(co -> Math.abs(co.next - co.pre) < esp).findFirst().orElse(null);

        if(sqrt == null) return -1;

        return sqrt.pre;
    }

    private double get(){
        return aproximateSqrt(this.base, this.esp, this.number);
    }

    private class Co {

        double pre;
        double next;

        Co(double pre, double next){
            this.pre = pre;
            this.next = next;
        }

    }

    public static void main(String[] args){

        DecimalFormat dec = new DecimalFormat("#0.00000000000000");
        Sqrt newSqrt = new Sqrt(2);
        System.out.println(dec.format(newSqrt.get()));
    }
}
